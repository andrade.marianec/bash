#!/bin/bash


d=$(date -d "next Wednesday" +"%Y-%m-%d")
e=$(date -d "$d + 7 days" +"%Y-%m-%d")

echo -e "Prox quarta: $d"
echo -e "Quarta seguinte: $e"

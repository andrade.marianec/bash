#!/bin/bash


echo "Nome do host: $(hostname)"
echo "Sistema Operacional: $(uname -a)"
echo "Usuário atual: $(whoami)"

echo "Modelo da CPU: $(cat /proc/cpuinfo | grep 'model name')"
echo "Núcelos da CPU: $(nproc)"

echo "Interfaces de Rede:"
ip addr sho

echo "Dispositivos USB:"
lsusb
